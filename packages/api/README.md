# API

API of a monitoring application

## Quick Start
**This project is created with yarn**

### Develop

1) Install dependencies

```shell
yarn install
```

2) Start database
This project uses a database in docker-compose

```shell
docker-compose up -d
```

3) Start server
```shell
yarn start
```
