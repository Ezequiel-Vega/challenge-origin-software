import request from 'supertest';
import App from '../../src/app';

const app = new App();

describe("Route Sign In", () => {
  beforeEach(async () => {
    await app.start();
  });

  afterEach(async () => {
    await app.stop();
  });

  it("should return a 200 response <Sign In success>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/signin')
      .send({
        username: "userMock",
        password: "userPassword"
      });

    expect(res.status).toBe(200);
    expect(res.body.error).toBeFalsy();
    expect(res.body.message).toBe("Sign in success");
  });

  it("should return a 500 response", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/signin')
      .send({
        username: "user",
        password: "userPassword"
      });

    expect(res.status).toBe(500);
    expect(res.body.error).toBeTruthy();
    expect(res.body.message).toBe("User not found");
  });

  it("should return a 200 response <Sign up success>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/signup')
      .send({
        username: "userMock2",
        password: "userPassword"
      });

    expect(res.status).toBe(200);
    expect(res.body.error).toBeFalsy();
    expect(res.body.message).toBe("Sign up success");
  });

  it("should return a 500 response <Password is required>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/signup')
      .send({
        username: "userMock2",
      });

    expect(res.status).toBe(500);
    expect(res.body.error).toBeTruthy();
    expect(res.body.message).toBe("Password is required");
  })

  it("should return a 500 response <User already exist>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/signup')
      .send({
        username: "userMock",
        password: "userPassword"
      });

    expect(res.status).toBe(500);
    expect(res.body.error).toBeTruthy();
    expect(res.body.message).toBe("User already exist");
  })
})
