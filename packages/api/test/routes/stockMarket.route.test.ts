import request from 'supertest';
import App from '../../src/app'

const app = new App();

describe("Stock Market", () => {
  beforeEach(async () => {
    await app.start();
  });

  afterEach(async () => {
    await app.stop();
  });

  it("POST /api/v1/stockmarket/add should return a 200 response <Add succefully>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .post('/api/v1/stockmarket/add')
      .send({
        user: 1,
        data: {
          symbol: 'AAPL',
          name: 'Apple Inc.',
          currency: 'USD'
        }
      })

    expect(res.status).toBe(200);
    expect(res.body.error).toBeFalsy();
    expect(res.body.message).toBe("Add successfully");
  })

  it("GET /api/v1/stockmarket/all should return a 200 response <Get all successfully>", async () => {
    const response = request(app.httpServer);

    const res = await response
      .get('/api/v1/stockmarket/all')
      .send({user: 1})

    expect(res.status).toBe(200);
    expect(res.body.error).toBeFalsy();
    expect(res.body.message).toBe("Get all successfully");
  });
})
