import { WinstonLogger } from '../../../src/services/Shared/infrastructure/LoggerWinston';
import Logger from '../../../src/services/Shared/domain/Logger';
import SignUpService from '../../../src/services/Auth/application/SignUpService';

import { UserMockRepository } from '../../../src/models/infrastructure/User/UserMockRepository';

const userMock = {
  username: "userMock2",
  password: "userPassword"
}

let logger: Logger;
let repository: UserMockRepository;

describe("Sign Up Service", () => {
  beforeAll(async () => {
    logger = WinstonLogger.getInstance();
    repository = new UserMockRepository();
  })

  it("should sign up success", async () => {
    const signInService = new SignUpService(logger, repository);
    const response = await signInService.run(userMock);
    const expectResponse = {
      message: 'Sign up success',
      error: false
    }

    const expectRepository = repository.userData[repository.userData.length - 1];

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
    expect(expectRepository.username).toBe(userMock.username);
  });
})
