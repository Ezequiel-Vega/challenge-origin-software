import { WinstonLogger } from '../../../src/services/Shared/infrastructure/LoggerWinston';
import Logger from '../../../src/services/Shared/domain/Logger';
import SignInService from '../../../src/services/Auth/application/SignInService';

import { UserMockRepository } from '../../../src/models/infrastructure/User/UserMockRepository';

const userMock = {
  username: "userMock",
  password: "userPassword"
}

let logger: Logger;
let repository: UserMockRepository;

describe("Sign In Service", () => {
  beforeAll(async () => {
    logger = WinstonLogger.getInstance();
    repository = new UserMockRepository();
  })

  it("should sign in success", async () => {
    const signInService = new SignInService(logger, repository);
    const response = await signInService.run(userMock);
    const expectResponse = {
      message: 'Sign in success',
      error: false
    }

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
  });

  it("should sign in Password invalid", async () => {
    const signInService = new SignInService(logger, repository);
    const response = await signInService.run({
      username: "userMock",
      password: "userPasswordFail"
    });
    const expectResponse = {
      message: 'Password invalid',
      error: true
    }

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
  });
})
