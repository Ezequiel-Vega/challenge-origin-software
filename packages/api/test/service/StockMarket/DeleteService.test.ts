import { WinstonLogger } from '../../../src/services/Shared/infrastructure/LoggerWinston';
import Logger from '../../../src/services/Shared/domain/Logger';

import DeleteStockMarketService from '../../../src/services/StockMarket/application/DeleteStockMarketService';

import { StockMarketsMockRepository } from '../../../src/models/infrastructure/StockMarket/StockMarketMockRespository';

let logger: Logger;
let repository: StockMarketsMockRepository;

describe("Delete Stock Market", () => {
  beforeAll(async () => {
    logger = WinstonLogger.getInstance();
    repository = new StockMarketsMockRepository();
  })

  it("should message <Delete successfully>", async () => {
    const service = new DeleteStockMarketService(logger, repository);
    const response = await service.run(1);

    const expectResponse = {
      message: 'Delete successfully',
      error: false
    }

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
  })
})
