import { WinstonLogger } from '../../../src/services/Shared/infrastructure/LoggerWinston';
import Logger from '../../../src/services/Shared/domain/Logger';

import AllStockMarketService from '../../../src/services/StockMarket/application/AllStockMarketService';

import { StockMarketsMockRepository } from '../../../src/models/infrastructure/StockMarket/StockMarketMockRespository';

let logger: Logger;
let repository: StockMarketsMockRepository;

describe("Add StockMarket", () => {
  beforeAll(async () => {
    logger = WinstonLogger.getInstance();
    repository = new StockMarketsMockRepository();
  })

  it("should message <Get all successfully>", async () => {
    const service = new AllStockMarketService(logger, repository);
    const response = await service.run();

    const expectResponse = {
      message: 'Get all successfully',
      error: false
    }

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
  })
})
