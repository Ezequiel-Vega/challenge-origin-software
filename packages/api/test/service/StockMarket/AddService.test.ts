import { WinstonLogger } from '../../../src/services/Shared/infrastructure/LoggerWinston';
import Logger from '../../../src/services/Shared/domain/Logger';

import AddStockMarketService from '../../../src/services/StockMarket/application/AddStockMarketService';

import { StockMarketsMockRepository } from '../../../src/models/infrastructure/StockMarket/StockMarketMockRespository';

let logger: Logger;
let repository: StockMarketsMockRepository;

describe("Add StockMarket", () => {
  beforeAll(async () => {
    logger = WinstonLogger.getInstance();
    repository = new StockMarketsMockRepository();
  })

  it("should message <Add succefully>", async () => {
    const service = new AddStockMarketService(logger, repository);
    const response = await service.run({
      symbol: 'AAPL',
      name: 'Apple Inc.',
      currency: 'USD'
    });

    const expectResponse = {
      message: 'Stock market added',
      error: false
    }

    expect(response.message.value).toBe(expectResponse.message);
    expect(response.error.value).toBe(expectResponse.error);
  })
})
