import Server from './server';

export default class App {
  server?: Server;

  async start() {
    const port = process.env.PORT || '8000';
    this.server = new Server(port);

    return this.server.listen()
  }

  async stop() {
    return this.server?.stop();
  }

  get httpServer() {
    return this.server?.getHttpServer();
  }
}
