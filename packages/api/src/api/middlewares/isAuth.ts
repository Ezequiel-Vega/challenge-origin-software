import jwtExpress from 'express-jwt';
import { Request } from 'express';

import config from '../../config';

const getTokenFromHeader = (req: Request): string => {
  if (
    (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') ||
    (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
  ) {
    return req.headers.authorization.split(' ')[1];
  } else {
    throw new Error('You cannot access this route');
  }
}

const isAuth = jwtExpress({
  secret: config.get('jwt.secretKey'),
  algorithms: [config.get('jwt.algorithm'), 'sha1', 'RS256', 'HS256'],
  userProperty: 'token',
  getToken: getTokenFromHeader,
});

export default isAuth;
