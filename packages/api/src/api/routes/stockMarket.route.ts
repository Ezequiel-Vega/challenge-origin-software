import { Router, Request, Response } from 'express';
import httpStatus from 'http-status';

// Loaders and config
import Logger from '../../services/Shared/domain/Logger';
import { WinstonLogger } from '../../services/Shared/infrastructure/LoggerWinston'
import config from '../../config';

import Token from '../../services/Shared/domain/Token';

import AddStockMarketService from '../../services/StockMarket/application/AddStockMarketService';
import AllStockMarketService from '../../services/StockMarket/application/AllStockMarketService';
import DeleteStockMarketService from '../../services/StockMarket/application/DeleteStockMarketService';

import { OriginStockMarketRepository } from '../../models/domain/OriginStockMarketRepository'
import StockMarketRepositoryFactory from '../../models/infrastructure/StockMarket/StockMarketRepositoryFactory';

import isAuth from '../middlewares/isAuth';

export const register = (router: Router) => {
  const logger: Logger = WinstonLogger.getInstance();
  let type: 'mock' | 'mysql';

  if (process.env.NODE_ENV === 'test') {
    type = 'mock';
  } else {
    type = 'mysql';
  }

  router.post(`/api/${config.get('api.version')}/stockmarket/add`, isAuth, async (req: Request, res: Response) => {
    logger.info('Calling stock market add endpoint');

    if (!req.header('Authorization')) {
      return res.status(httpStatus.UNAUTHORIZED).send({ error: { value: true, message: 'Token not found' } });
    }

    const user = Token.getUser(req.header('Authorization'));
    const repository: OriginStockMarketRepository = StockMarketRepositoryFactory.create(type, user);

    const service = new AddStockMarketService(logger, repository);
    const response = await service.run(req.body.data);

    if (response.error.value) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send(response.toPrimitive());
    }

    return res.status(httpStatus.OK).send(response.toPrimitive());
  });

  router.get(`/api/${config.get('api.version')}/stockmarket/all`, isAuth, async (req: Request, res: Response) => {
    logger.info('Calling stock market all endpoint');

    if (!req.header('Authorization')) {
      return res.status(httpStatus.UNAUTHORIZED).send({ error: { value: true, message: 'Token not found' } });
    }

    const user = Token.getUser(req.header('Authorization'));
    const repository: OriginStockMarketRepository = StockMarketRepositoryFactory.create(type, user);

    const service = new AllStockMarketService(logger, repository);
    const response = await service.run();

    if (response.error.value) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send(response.toPrimitive());
    }

    return res.status(httpStatus.OK).send(response.toPrimitive());
  });

  router.delete(`/api/${config.get('api.version')}/stockmarket/delete/:id`, isAuth, async (req: Request, res: Response) => {
    logger.info('Calling stock market delete endpoint');

    if (!req.header('Authorization')) {
      return res.status(httpStatus.UNAUTHORIZED).send({ error: { value: true, message: 'Token not found' } });
    }

    const user = Token.getUser(req.header('Authorization'));
    const repository: OriginStockMarketRepository = StockMarketRepositoryFactory.create(type, user);

    const service = new DeleteStockMarketService(logger, repository);
    const response = await service.run(parseInt(req.params.id));

    if (response.error.value) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send(response.toPrimitive());
    }

    return res.status(httpStatus.OK).send(response.toPrimitive());
  });
}
