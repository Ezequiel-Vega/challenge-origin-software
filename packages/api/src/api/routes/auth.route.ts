import { Router, Request, Response } from 'express';
import httpStatus from 'http-status';

// Loaders and config
import Logger from '../../services/Shared/domain/Logger';
import { WinstonLogger } from '../../services/Shared/infrastructure/LoggerWinston'
import config from '../../config';

import SignInService from '../../services/Auth/application/SignInService';
import SignUpService from '../../services/Auth/application/SignUpService';

import { OriginUserRepository } from '../../models/domain/OriginUserRepository'
import UserRepositoryFactory from '../../models/infrastructure/User/UserRepositoryFactory';

export const register = (router: Router) => {
  const logger: Logger = WinstonLogger.getInstance();
  let repository: OriginUserRepository;

  if (process.env.NODE_ENV === 'test') {
    repository = UserRepositoryFactory.create('mock');
  } else {
    repository = UserRepositoryFactory.create('mysql');
  }

  router.post(`/api/${config.get('api.version')}/signin`, async (req: Request, res: Response) => {
    logger.info('Calling sign in endpoint');

    const service = new SignInService(logger, repository);
    const response = await service.run(req.body);

    if (response.error.value) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send(response.toPrimitive());
    }

    return res.status(httpStatus.OK).send(response.toPrimitive());
  });

  router.post(`/api/${config.get('api.version')}/signup`, async (req: Request, res: Response) => {
    logger.info('Calling sign up endpoint');

    const service = new SignUpService(logger, repository);
    const response = await service.run(req.body);

    if (response.error.value) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send(response.toPrimitive());
    }

    return res.status(httpStatus.OK).send(response.toPrimitive());
  });
}
