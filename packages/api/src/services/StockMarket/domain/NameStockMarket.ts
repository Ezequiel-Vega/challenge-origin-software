import { InvalidArgumentError } from '../../Shared/domain/value-object/InvalidArgumentError';
import { StringValueObject } from "../../Shared/domain/value-object/StringValueObject";

export default class NameStockMarket extends StringValueObject {
  constructor(value: string) {
    super(value);
    this.ensureLengthIsLessThan150Characters(value);
  }

  private ensureLengthIsLessThan150Characters(value: string): void {
    if (value.length > 150) {
      throw new InvalidArgumentError(`The Name <${value}> has more than 150 characters`);
    }
  }
}
