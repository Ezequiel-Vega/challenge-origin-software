import SymbolStockMarket from './SymbolStockMarket';
import NameStockMarket from './NameStockMarket';
import CurrencyStockMarket from './CurrencyStockMarket';

export type stockMarketTextPlain = {
  symbol: string;
  name: string;
  currency: string;
  ID?: number;
}

export default class StockMarket {
  readonly symbol: SymbolStockMarket;
  readonly name: NameStockMarket;
  readonly currency: CurrencyStockMarket;
  readonly ID?: number;

  constructor(symbol: SymbolStockMarket, name: NameStockMarket, currency: CurrencyStockMarket, ID?: number) {
    this.symbol = symbol;
    this.name = name;
    this.currency = currency;
    this.ID = ID;
  }

  static create(symbol: SymbolStockMarket, name: NameStockMarket, currency: CurrencyStockMarket, ID?: number): StockMarket {
    return new StockMarket(symbol, name, currency, ID);
  }

  static fromPrimitives(data: stockMarketTextPlain): StockMarket {
    return new StockMarket(
      new SymbolStockMarket(data.symbol),
      new NameStockMarket(data.name),
      new CurrencyStockMarket(data.currency),
      data.ID
    );
  }

  toPrimitives(): stockMarketTextPlain {
    return {
      symbol: this.symbol.value,
      name: this.name.value,
      currency: this.currency.value,
      ID: this.ID ? this.ID : 0
    };
  }
}
