import { InvalidArgumentError } from '../../Shared/domain/value-object/InvalidArgumentError';
import { StringValueObject } from "../../Shared/domain/value-object/StringValueObject";

export default class SymbolStockMarket extends StringValueObject {
  constructor(value: string) {
    super(value);
    this.ensureLengthIsLessThan10Characters(value);
  }

  private ensureLengthIsLessThan10Characters(value: string): void {
    if (value.length > 10) {
      throw new InvalidArgumentError(`The Symbol <${value}> has more than 10 characters`);
    }
  }
}
