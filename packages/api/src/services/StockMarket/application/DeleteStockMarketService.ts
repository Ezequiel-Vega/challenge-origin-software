import Logger from '../../Shared/domain/Logger';
import Response from '../../Shared/domain/Response';

import { OriginStockMarketRepository } from '../../../models/domain/OriginStockMarketRepository';

export default class DeleteStockMarketService {
  private logger: Logger;

  private repository: OriginStockMarketRepository;

  constructor(logger: Logger, repository: OriginStockMarketRepository) {
    this.logger = logger;
    this.repository = repository;
  }

  async run(idStockMarket: number): Promise<Response<void>> {
    this.logger.info("delete favorite stock market");
    await this.repository.deleteById(idStockMarket);

    this.logger.info("Return response");
    return Response.fromPrimitives({error: false, message: "Delete successfully"});
  }
}
