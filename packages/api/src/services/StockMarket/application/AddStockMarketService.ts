import Logger from '../../Shared/domain/Logger';
import Response from '../../Shared/domain/Response';

import StockMarket, { stockMarketTextPlain } from '../domain/StockMarket';

import { OriginStockMarketRepository } from '../../../models/domain/OriginStockMarketRepository';

export default class AddStockMarketService {
  private logger: Logger;

  private repository: OriginStockMarketRepository;

  constructor(logger: Logger, repository: OriginStockMarketRepository) {
    this.logger = logger;
    this.repository = repository;
  }

  async run(data: stockMarketTextPlain): Promise<Response<void>> {
    this.logger.info("Check data");
    const stockMarket = StockMarket.fromPrimitives(data);

    this.logger.info("Add favorite stock market");
    await this.repository.add(stockMarket);

    this.logger.info("Return response");
    return Response.fromPrimitives({error: false, message: "Stock market added"});
  }
}
