import Logger from '../../Shared/domain/Logger';
import Response from '../../Shared/domain/Response';

import { OriginStockMarketRepository } from '../../../models/domain/OriginStockMarketRepository';
import { stockMarketTextPlain } from '../domain/StockMarket';

export default class AllStockMarketService {
  private logger: Logger;

  private repository: OriginStockMarketRepository;

  constructor(logger: Logger, repository: OriginStockMarketRepository) {
    this.logger = logger;
    this.repository = repository;
  }

  async run(): Promise<Response<void>> {
    this.logger.info("Get all favorite stock market");
    const response = await this.repository.getAll();

    if (response.error.value || !response.data) {
      return Response.fromPrimitives({error: true, message: response.message.value});
    }

    this.logger.info("Return response");
    let stockMarkets: stockMarketTextPlain[] = [];
    response.data.forEach((stockMarket) => {
      stockMarkets.push(stockMarket.toPrimitives());
    });

    return Response.fromPrimitives({error: false, message: "Get all successfully", data: stockMarkets});
  }
}
