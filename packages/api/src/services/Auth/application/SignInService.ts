import Logger from '../../Shared/domain/Logger';
import Response from '../../Shared/domain/Response';
import Token from '../../Shared/domain/Token';

import User from '../../User/domain/User';

import { OriginUserRepository } from '../../../models/domain/OriginUserRepository';

type signInPlain = {
  username: string;
  password: string;
}

export default class SignInService {
  private logger: Logger;

  private repository: OriginUserRepository;

  constructor(logger: Logger, repository: OriginUserRepository) {
    this.logger = logger;
    this.repository = repository;
  }

  async run(data: signInPlain): Promise<Response<Token>>{
    this.logger.info("Check user data");
    const user = User.fromPrimitives(data);

    this.logger.info("Find user in DB");
    const response = await this.repository.getByUsername(user.username);

    if (response.error.value || !response.data) {
      this.logger.error(response.message.value);
      return Response.fromPrimitives({error: true, message: response.message.value});
    }

    this.logger.info("Check password");
    const checkPassword = await response.data.password.validate(data.password);
    if (!checkPassword) {
      return Response.fromPrimitives({
        error: true,
        message: 'Password invalid'
      });
    }

    this.logger.info('Create Token');
    const token = Token.generate(response.data.toPrimitives());

    return Response.fromPrimitives({message: "Sign in success", error: false, data: token});
  }
}
