import Logger from '../../Shared/domain/Logger';
import Response from '../../Shared/domain/Response';

import User, { userPlainText } from '../../User/domain/User'
import PasswordUser from '../../User/domain/PasswordUser'

import { OriginUserRepository } from '../../../models/domain/OriginUserRepository';

export default class SignUpService {
  private logger: Logger;

  private repository: OriginUserRepository;

  constructor(logger: Logger, repository: OriginUserRepository) {
    this.logger = logger;
    this.repository = repository;
  }

  async run(data: userPlainText): Promise<Response<void>>{
    this.logger.info("Check data");

    if (!data.password) {
      return Response.fromPrimitives({error: true, message: "Password is required"});
    }

    const encryptPassword = await PasswordUser.encrypt(data.password);
    const user = User.fromPrimitives({username: data.username, password: encryptPassword});

    this.logger.info("Check if user exist");
    const findUser = await this.repository.getByUsername(user.username);

    if (findUser.data) {
      return Response.fromPrimitives({error: true, message: "User already exist"});
    }

    this.logger.info("Create user");
    await this.repository.add(user);

    this.logger.info("Return response");
    return Response.fromPrimitives({error: false, message: "Sign up success"});
  }
}
