import UsernameUser from './UsernameUser';
import PasswordUser from './PasswordUser';

export interface userPlainText {
  id?: number;
  username: string;
  password: string;
}

export default class User {
  readonly username: UsernameUser;
  readonly password: PasswordUser;
  readonly id?: number;

  constructor(username: UsernameUser, password: PasswordUser, id?: number) {
    this.username = username;
    this.password = password;
    this.id = id;
  }

  static create(username: UsernameUser, password: PasswordUser, id?: number): User {
    return new User(username, password, id);
  }

  static fromPrimitives(data: userPlainText): User {
    return User.create(
      new UsernameUser(data.username),
      new PasswordUser(data.password),
      data.id
    );
  }

  toPrimitives(): userPlainText {
    return {
      username: this.username.value,
      password: this.password.value,
      id: this.id ? this.id : 0
    }
  }
}
