import { genSalt, hash, compare } from 'bcrypt';

import { InvalidArgumentError } from "../../Shared/domain/value-object/InvalidArgumentError";
import { StringValueObject } from "../../Shared/domain/value-object/StringValueObject";

export default class PasswordUser extends StringValueObject {
  constructor(value: string) {
    super(value);
    this.ensureLengthIsLessThan150Characters(value);
  }

  private ensureLengthIsLessThan150Characters(value: string): void {
    if (value.length > 150) {
      throw new InvalidArgumentError(`The password <${value}> has more than 150 characters`);
    }
  }

  static async encrypt(value: string): Promise<string> {
    const salt = await genSalt(10);
    return await hash(value, salt);
  }

  async validate(value: string): Promise<boolean> {
    return await compare(value, this.value);
  }
}
