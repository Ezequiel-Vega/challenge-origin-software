export abstract class BooleanValueObject {
  readonly value: boolean;

  constructor(value: boolean) {
    this.value = value;
  }

  toBooelan(): boolean {
    if (typeof this.value !== 'boolean') {
      return Boolean(this.value)
    } else {
      return this.value;
    }
  }
}
