import MessageResponse from './MessageResponse';
import ErrorResponse from './ErrorResponse'

type plainText = {
  message: string;
  error: boolean;
  data?: any;
}

export default class Response<T> {
  readonly message: MessageResponse;
  readonly error: ErrorResponse;
  readonly data?: T;

  constructor(message: MessageResponse, error: ErrorResponse, data: T) {
    this.message = message;
    this.error = error;
    this.data = data;
  }

  static create(message: MessageResponse, error: ErrorResponse, data?: any): Response<any> {
    return new Response(message, error, data);
  }

  static fromPrimitives<T>(data: plainText): Response<T> {
    return Response.create(
      new MessageResponse(data.message),
      new ErrorResponse(data.error),
      data.data
    );
  }

  toPrimitive(): plainText {
    return {
      message: this.message.value,
      error: this.error.value,
      data: this.data ? this.data : undefined
    }
  }
}
