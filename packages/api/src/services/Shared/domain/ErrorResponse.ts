import { BooleanValueObject } from "./value-object/BooleanValueObject";

export default class ErrorResponse extends BooleanValueObject {}
