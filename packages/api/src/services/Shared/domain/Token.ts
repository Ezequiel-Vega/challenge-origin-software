import jwt from 'jsonwebtoken'
import config from '../../../config'
import { userPlainText } from '../../user/domain/User'

interface TokenUser extends userPlainText {
  id: number;
}
export default class Token {
  static generate(data: userPlainText): string {
    const secretKey = config.get('jwt.secretKey').replace(/\\n/gm, '\n')
    return jwt.sign(data, secretKey, { expiresIn: 60 * 60 * 21  });
  }

  static getUser(token: string | undefined): number {
    if (!token) {
      return -1;
    }
    const tokenSplit = token.split(' ')[1];
    const secretKey = config.get('jwt.secretKey').replace(/\\n/gm, '\n')
    const decoded = jwt.verify(tokenSplit, secretKey) as TokenUser;
    return decoded.id
  }
}
