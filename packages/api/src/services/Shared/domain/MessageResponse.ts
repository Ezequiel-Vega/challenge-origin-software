import { InvalidArgumentError } from "./value-object/InvalidArgumentError";
import { StringValueObject } from "./value-object/StringValueObject";

export default class MessageResponse extends StringValueObject {
  constructor(value: string) {
    super(value);
    this.ensureLengthIsLessThan200Characters(value);
  }

  private ensureLengthIsLessThan200Characters(value: string) {
    if (value.length > 200) {
      throw new InvalidArgumentError(`The message <${value}> has more than 200 characters`);
    }
  }
}
