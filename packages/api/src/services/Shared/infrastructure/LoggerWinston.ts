/* eslint-disable no-unused-vars */
import config from '../../../config';
import winston, { Logger as LoggerWinston } from 'winston';
import Logger from '../domain/Logger';

enum Levels {
  INFO = 'info',
  DEBUG = 'debug',
  ERROR = 'error'
}

export class WinstonLogger implements Logger {
  private logger: LoggerWinston;

  // eslint-disable-next-line no-use-before-define
  private static winstonLogger: WinstonLogger;

  constructor() {
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.timestamp({
          format: 'DD-MM-YYYY HH:mm:ss'
        }),
        winston.format.prettyPrint(),
        winston.format.errors({stack: true}),
        winston.format.splat(),
        winston.format.colorize(),
        winston.format.simple()
      ),
      transports: [
        new winston.transports.Console({
          silent: config.get('env') === 'test'
        }),
        new winston.transports.File({ filename: `logs/${Levels.INFO}.log`, level: Levels.INFO }),
        new winston.transports.File({ filename: `logs/${Levels.DEBUG}.log`, level: Levels.DEBUG }),
        new winston.transports.File({ filename: `logs/${Levels.ERROR}.log`, level: Levels.ERROR })
      ]
    });
  }

  info(msg: string): void {
    this.logger.info(msg);
  }

  debug(msg: string): void {
    this.logger.debug(msg);
  }

  error(msg: string | Error): void {
    this.logger.error(msg);
  }

  static getInstance(): WinstonLogger {
    if (!WinstonLogger.winstonLogger) {
      WinstonLogger.winstonLogger = new WinstonLogger();
    }

    return WinstonLogger.winstonLogger;
  }
}
