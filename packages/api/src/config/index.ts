import { resolve } from 'path';
import convict from 'convict';

const config = convict({
  env: {
    doc: 'The application enviroment',
    format: ['production', 'dev', 'test'],
    default: 'default',
    env: 'NODE_ENV'
  },
  api: {
    version: {
      doc: 'The version api',
      format: String,
      env: 'API_VERSION',
      default: 'v1'
    }
  },
  jwt: {
    secretKey: {
      doc: 'The secret key JWT',
      format: String,
      env: 'JWT_SECRET_KEY',
      default: 'p8293eoijpe8ur%089u02oh34(@*(&jhk892*$'
    },
    algorithm: {
      doc: 'The algorithm JWT',
      format: String,
      env: 'JWT_ALGORITHM',
      default: 'RS256'
    }
  },
  database: {
    user: {
      doc: 'The user Data Base',
      format: String,
      env: 'DB_USER',
      default: 'root'
    },
    password: {
      doc: 'The password Data Base',
      format: String,
      env: 'DB_PASSWORD',
      default: 'root',
    },
    host: {
      doc: 'The host Data Base',
      format: String,
      env: 'DB_HOST',
      default: 'localhost'
    },
    port: {
      doc: 'The port Data Base',
      format: Number,
      env: 'DB_PORT',
      default: 3306
    },
    database: {
      doc: 'The database Data Base',
      format: String,
      env: 'DB_DATABASE',
      default: 'origin_challen'
    }
  }
});

const pathFileEnv = resolve(__dirname, `${config.get('env')}.json`);
const pathFileDefault = resolve(__dirname, './default.json');

config.loadFile([pathFileEnv, pathFileDefault]);
config.validate({ allowed: 'strict' });

export default config;
