import MariaDBRepository from "../Shared/MariaDBRepository";
import { OriginStockMarketRepository } from "../../domain/OriginStockMarketRepository";
import { StockMarketQuery } from '../../domain/StockMarket'

import StockMarket from '../../../services/StockMarket/domain/StockMarket';

import Response from '../../../services/Shared/domain/Response';

export class StockMarketMySQLRepository extends MariaDBRepository implements OriginStockMarketRepository {
  private user: number;

  constructor(user: number) {
    super();
    this.user = user;
  }

  async add(data: StockMarket): Promise<void> {
    const conn = this.getConnection();

    const findSymbol = await this.getBySymbol(data.symbol.value);

    if (!findSymbol.error.value) {
      throw Error('Cannot add the same asset');
    }

    await conn.query(`INSERT INTO favoritesStockMarket(symbol, name, currency, userId) VALUES ('${data.symbol.value}', '${data.name.value}', '${data.currency.value}', ${this.user})`);
  }

  async getAll(): Promise<Response<StockMarket[]>> {
    const conn = this.getConnection();

    const [ stockMarketRow ] = await conn.query<StockMarketQuery[]>(`SELECT * FROM stockMarketView WHERE user = ${this.user}`);

    await this.endConneciton();

    if (!stockMarketRow) {
      return Response.fromPrimitives({error: true, message: 'Stock market not found!'});
    }

    const stockMarket = stockMarketRow.map(stockMarket => {
      return StockMarket.fromPrimitives(stockMarket);
    });

    return Response.fromPrimitives({error: false, message: 'Stock market found', data: stockMarket});
  }

  async getById(id: number): Promise<Response<StockMarket>> {
    const conn = this.getConnection();

    const [ stockMarketRow ] = await conn.query<StockMarketQuery[]>(`SELECT * FROM favoritesStockMarket WHERE id = ${id}`);

    await this.endConneciton();

    if (!stockMarketRow[0]) {
      return Response.fromPrimitives({error: true, message: 'Stock market not found!'});
    }

    return Response.fromPrimitives({error: false, message: 'Stock market found', data: StockMarket.fromPrimitives(stockMarketRow[0])});
  }

  async getBySymbol(symbol: string): Promise<Response<StockMarket>> {
    const conn = this.getConnection();

    const [ stockMarketRow ] = await conn.query<StockMarketQuery[]>(`SELECT * FROM stockMarketView WHERE symbol = '${symbol}' and user = ${this.user}`);

    await this.endConneciton();

    if (!stockMarketRow[0]) {
      return Response.fromPrimitives({error: true, message: 'Stock market not found!'});
    }

    const parseStockMarket = {
      symbol: stockMarketRow[0].symbol,
      name: stockMarketRow[0].name,
      currency: stockMarketRow[0].currency
    }

    return Response.fromPrimitives({error: false, message: 'Stock market found', data: StockMarket.fromPrimitives(parseStockMarket)});
  }

  async deleteById(id: number): Promise<void> {
    const stockMarket = await this.getById(id);

    if (stockMarket.error.value) {
      throw Error("Can't delete an asset that doesn't exist");
    }

    const conn = this.getConnection();

    await conn.query(`DELETE FROM favoritesStockMarket WHERE id = ${id}`);

    await this.endConneciton();
  }
}
