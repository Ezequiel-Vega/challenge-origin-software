import { OriginStockMarketRepository } from "../../domain/OriginStockMarketRepository";

import StockMarket from '../../../services/StockMarket/domain/StockMarket';

import Response from '../../../services/Shared/domain/Response';

type stockMarket = {
  symbol: string;
  name: string;
  currency: string;
}
export class StockMarketsMockRepository implements OriginStockMarketRepository {
  private stockMarkets: stockMarket[] = [{
    symbol: 'APPL',
    name: 'Apple Inc.',
    currency: 'USD'
  }]

  add(data: StockMarket): Promise<void> {
    this.stockMarkets.push(data.toPrimitives());
    return Promise.resolve();
  }

  getAll(): Promise<Response<StockMarket[]>> {
    const stockMarkets = this.stockMarkets.map(market => StockMarket.fromPrimitives(market));
    return Promise.resolve(Response.fromPrimitives({error: false, message: 'Stock market found', data: stockMarkets}));
  }

  getById(id: number): Promise<Response<StockMarket>> {
    return Promise.resolve(Response.fromPrimitives({error: true, message: "Stock market found", data: StockMarket.fromPrimitives(this.stockMarkets[0])}));
  }

  getBySymbol(symbol: string): Promise<Response<StockMarket>> {
    const findStockMarket = this.stockMarkets.find(market => market.symbol === symbol);

    if (!findStockMarket) {
      return Promise.resolve(Response.fromPrimitives({error: true, message: "Stock market not found"}));
    }

    return Promise.resolve(Response.fromPrimitives({error: false, message: 'Stock market found', data: findStockMarket}));
  }

  deleteById(id: number): Promise<void> {
    this.stockMarkets.pop();
    return Promise.resolve();
  }
}
