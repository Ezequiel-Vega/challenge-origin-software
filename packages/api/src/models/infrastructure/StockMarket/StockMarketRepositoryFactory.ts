import { OriginStockMarketRepository } from '../../domain/OriginStockMarketRepository';
import { StockMarketsMockRepository } from './StockMarketMockRespository';
import { StockMarketMySQLRepository } from './StockMarketMySQLRepository'

type Connection = 'mock' | 'mysql';

export default class UserRepositoryFactory {
  static create(connection: Connection, user: number): OriginStockMarketRepository {
    switch(connection) {
      case 'mock':
        return new StockMarketsMockRepository();
      case 'mysql':
        return new StockMarketMySQLRepository(user);
      default: {
        throw Error('Invalid connection');
      }
    }
  }
}
