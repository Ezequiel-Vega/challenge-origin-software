import { createPool, Pool } from "mysql2/promise";
import config from '../../../config';

export default class MariaDBRepository {
  private conntection: Pool;

  // eslint-disable-next-line no-use-before-define
  private static instance: MariaDBRepository;

  public getConnection(): Pool {
    this.conntection = createPool({
      host: config.get('database.host'),
      port: config.get('database.port'),
      user: config.get('database.user'),
      password: config.get('database.password'),
      database: config.get('database.database'),
    });

    return this.conntection;
  }

  public async endConneciton(): Promise<void> {
    try {
      await this.conntection.end();
    } catch (e) {
      console.log(e);
    }
  }

  static getInstance(): MariaDBRepository {
    if (!MariaDBRepository.instance) {
      MariaDBRepository.instance = new MariaDBRepository();
    }

    return MariaDBRepository.instance;
  }
}
