import { OriginUserRepository } from '../../domain/OriginUserRepository';
import { UserMockRepository } from './UserMockRepository';
import { UserMySQLRepository } from './UserMySQLRepository'

type Connection = 'mock' | 'mysql';

export default class UserRepositoryFactory {
  static create(connection: Connection): OriginUserRepository {
    switch(connection) {
      case 'mock':
        return new UserMockRepository();
      case 'mysql':
        return new UserMySQLRepository();
      default: {
        throw Error('Invalid connection');
      }
    }
  }
}
