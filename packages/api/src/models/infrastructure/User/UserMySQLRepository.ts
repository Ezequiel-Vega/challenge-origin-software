import { Pool, RowDataPacket } from 'mysql2/promise';

import { OriginUserRepository } from "../../domain/OriginUserRepository";
import MariaDBRepository from "../Shared/MariaDBRepository";

import Response from '../../../services/Shared/domain/Response';
import User from "../../../services/User/domain/User";
import UsernameUser from '../../../services/User/domain/UsernameUser';

interface UserQuery extends RowDataPacket {
  id: number;
  username: string;
  password: string;
}

export class UserMySQLRepository extends MariaDBRepository implements OriginUserRepository {
  async add(data: User): Promise<void> {
    const dataUser = data.toPrimitives();

    const conn: Pool = this.getConnection();

    await conn.query(`INSERT INTO users (username, password) VALUES ('${dataUser.username}', '${dataUser.password}')`);
  }

  getAll(): Promise<Response<User[]>> {
    throw new Error("Method not implemented.");
  }

  getById(id: number): Promise<Response<User>> {
    throw new Error("Method not implemented.");
  }

  async getByUsername(username: UsernameUser): Promise<Response<User>> {
    const conn: Pool = this.getConnection();

    const [ userRow ] = await conn.query<UserQuery[]>(`SELECT * FROM users WHERE username = '${username.value}'`);

    await this.endConneciton();

    if (!userRow[0]) {
      return Response.fromPrimitives({error: true, message: 'User not found!'});
    }

    const userParsed = {
      username: userRow[0].username,
      password: userRow[0].password,
      id: userRow[0].ID
    }

    return Response.fromPrimitives({error: false, message: 'User found', data: User.fromPrimitives(userParsed)});
  }

  deleteById(id: number): Promise<void> {
    throw new Error("Method not implemented.");
  }
}
