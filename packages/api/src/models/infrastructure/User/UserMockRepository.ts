import { OriginUserRepository } from "../../domain/OriginUserRepository";

import Response from '../../../services/Shared/domain/Response';
import User, { userPlainText } from "../../../services/User/domain/User";
import UsernameUser from '../../../services/User/domain/UsernameUser';

export class UserMockRepository implements OriginUserRepository {
  private data: userPlainText[] = [
    {
      username: 'userMock',
      password: '$2b$10$sqjIBQGbo4CFl8VqRhk7xeLLQjgWfXUQWPPNRoMiLJoj.SSCbEvmG'
    }
  ];

  get userData() : userPlainText[] {
    return this.data;
  }

  add(data: User): Promise<void> {
    this.data.push(data.toPrimitives());
    return Promise.resolve();
  }

  getAll(): Promise<Response<User[]>> {
    throw new Error("Method not implemented.");
  }

  getById(id: number): Promise<Response<User>> {
    throw new Error("Method not implemented.");
  }

  getByUsername(username: UsernameUser): Promise<Response<User>> {
    const findUser = this.data.find(userData => userData.username === username.value);

    if (!findUser) {
      return Promise.resolve(Response.fromPrimitives({error: true, message: 'User not found'}));
    }

    const user = User.fromPrimitives(findUser);

    const response = Response.fromPrimitives<User>({error: false, message: '', data: user})

    return Promise.resolve(response);
  }

  deleteById(id: number): Promise<void> {
    throw new Error("Method not implemented.");
  }
}
