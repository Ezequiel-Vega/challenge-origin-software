import { OriginRepository } from "./OriginRepository";

import Response from '../../services/Shared/domain/Response';
import User from "../../services/User/domain/User";
import UsernameUser from "../../services/User/domain/UsernameUser";

export abstract class OriginUserRepository extends OriginRepository<User> {
  abstract getByUsername(username: UsernameUser): Promise<Response<User>>;
}
