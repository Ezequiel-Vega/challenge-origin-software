import { RowDataPacket } from 'mysql2/promise';

export interface StockMarketQuery extends RowDataPacket {
  symbol: string;
  name: string;
  currency: string;
}
