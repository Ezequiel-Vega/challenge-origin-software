import { OriginRepository } from "./OriginRepository";

import Response from '../../services/Shared/domain/Response';
import StockMarket from "../../services/StockMarket/domain/StockMarket";

export abstract class OriginStockMarketRepository extends OriginRepository<StockMarket> {
  abstract getBySymbol(symbol: string): Promise<Response<StockMarket>>;
}

