import Response from '../../services/Shared/domain/Response';

export abstract class OriginRepository<T> {
  abstract add(data: T): Promise<void>;

  abstract getAll(): Promise<Response<T[]>>;

  abstract getById(id: number): Promise<Response<T>>;

  abstract deleteById(id: number): Promise<void>;
}
