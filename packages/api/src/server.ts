import bodyParser from 'body-parser';
import compression from 'compression';
import errorHandler from 'errorhandler';
import express from 'express';
import Router from 'express-promise-router';
import helmet from 'helmet';
import cors from 'cors';
import * as http from 'http';
import Logger from './services/Shared/domain/Logger';
import { WinstonLogger } from './services/Shared/infrastructure/LoggerWinston';
import { registerRoutes } from './api';

export default class Server {
  private express: express.Express;
  private port: string;
  private httpServer?: http.Server;
  private logger: Logger;

  constructor(port: string) {
    this.port = port;
    this.logger = WinstonLogger.getInstance()
    this.express = express();
    this.express.use(cors());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: true}));
    this.express.use(helmet.xssFilter());
    this.express.use(helmet.noSniff());
    this.express.use(helmet.hidePoweredBy());
    this.express.use(helmet.frameguard({action: 'DENY'}));
    this.express.use(compression());

    const router = Router();
    router.use(errorHandler());
    this.express.use(router);

    registerRoutes(router);
  }

  async listen(): Promise<void> {
    return new Promise(resolve => {
      this.httpServer = this.express.listen(this.port, () => {
        this.logger.info(
          `🚀 Origin backend app is running at http://localhost:${this.port} in ${this.express.get('env')} mode`
        );
        this.logger.info('Press CTRL-C to stop\n');
        resolve();
      })
    })
  }

  getHttpServer() {
    return this.httpServer;
  }

  async stop(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.httpServer) {
        this.httpServer.close(error => {
          if (error) {
            return reject(error);
          }
          return resolve()
        })
      }
      return resolve();
    })
  }
}
