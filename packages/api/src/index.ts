import App from './App';

try {
  new App().start();
} catch (e) {
  console.log(e);
  process.exit(1);
}

process.on('uncaughtException', (error) => {
  console.log('uncaughtException', error);
  process.exit(1);
})
