CREATE VIEW stockMarketView
AS
(
  SELECT
  fsm.ID,
  fsm.symbol,
  fsm.name,
  fsm.currency,
  u.ID AS user
  FROM users u
  INNER JOIN favoritesStockMarket fsm
  ON fsm.userId = u.ID
  ORDER BY fsm.currency
);
