import Client from "../client";
import CacheDecoratorClient from '../client/cache-decorator-client';
import MockClient from "../client/mock-client";
import RestClient from "../client/rest-client";

interface Config {
  apiBaseUrl: string;
  clientType: 'mock' | 'rest'
}

class _AppConfig {
  private defaultInstance: Config = {
    apiBaseUrl: 'http://localhost:8000/api/v1',
    clientType: 'rest'
  }

  private getInstance(): Config{
    let result = (window as any).serverconfig;
    if (!result) {
      result = this.defaultInstance;
    }

    return result;
  }

  isDevelopEnv(): boolean {
    const config = this.getInstance()
    return config.clientType === 'mock';
  }

  buildClient(): Client {
    const config = this.getInstance();
    let result: Client;
    if(config.clientType === 'rest') {
      result = new RestClient(config.apiBaseUrl);
    } else {
      result = new MockClient();
    }

    return new CacheDecoratorClient(result);
  }
}

const AppConfig = new _AppConfig();

export default AppConfig;
