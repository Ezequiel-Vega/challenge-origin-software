import Client, { StockMarket, AccountInfo } from "..";

export default class MockClient implements Client {
  async fetchAccountInfo(): Promise<AccountInfo> {
    throw new Error("Method not implemented.");
  }

  async addStockMarket(stockMarket: StockMarket): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async fetchStockMarketByName(name: string): Promise<StockMarket> {
    throw new Error("Method not implemented.");
  }

  async fetchAllStockMarkets(): Promise<StockMarket[]> {
    throw new Error("Method not implemented.");
  }

  async deleteStockMarket(id: number): Promise<void> {
    throw new Error("Method not implemented.");
  }

  onSessionExpired(callback: () => void): () => void {
    return callback;
  }
}
