import axios, { AxiosResponse, AxiosInstance } from "axios";
import Client, { StockMarket, AccountInfo } from "..";

export default class RestClient implements Client {
  private baseUrl: string;
  private axios: AxiosInstance = axios;
  private _onSessionExpired: any;

  private checkResponseForSessionExpired = <T>(error: {
    response?: AxiosResponse<T>;
  }): Promise<{ response?: AxiosResponse<T> }> => {
    if ((error.response && error.response.status === 500) || 401) {
      this.sessionExpired();
    }
    return Promise.reject(error);
  };

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
    this.axios.interceptors.response.use(
      (r: any) => r,
      (r: any) => this.checkResponseForSessionExpired(r)
    );
  }

  async fetchAccountInfo(): Promise<AccountInfo> {
    throw new Error("Method not implemented.");
  }

  async addStockMarket(stockMarket: StockMarket): Promise<void> {
    axios.post(`${this.baseUrl}/stockMarket/add`,
    {data: stockMarket},
    {
      headers: {
        Authorization: `Token ${localStorage.getItem("token")}`,
      }
    })
    .catch(e => console.log(e));
  }

  async fetchStockMarketByName(name: string): Promise<StockMarket> {
    throw new Error("Method not implemented.");
  }

  async fetchAllStockMarkets(): Promise<StockMarket[]> {
    let stockMarkets: StockMarket[] = [];

    const response = await this.axios
      .get(`${this.baseUrl}/stockmarket/all`, {
        headers: {
          Authorization: `Token ${localStorage.getItem("token")}`,
        },
      })

    if (response.data) {
      response.data.data.forEach((stockMarket: StockMarket) => {
        stockMarkets.push(stockMarket);
      });
    }

    return stockMarkets;
  }

  async deleteStockMarket(id: number): Promise<void> {
    axios.delete(`${this.baseUrl}/stockMarket/delete/${id}`,
    {
      headers: {
        Authorization: `Token ${localStorage.getItem("token")}`,
      }
    })
    .catch(e => console.log(e));
  }

  onSessionExpired(callback: () => void): () => void {
    if (callback) {
      this._onSessionExpired = callback;
    }

    return this._onSessionExpired;
  }

  private sessionExpired() {
    if (this._onSessionExpired) {
      this._onSessionExpired();
    }
  }
}
