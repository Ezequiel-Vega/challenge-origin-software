import React from "react";
import { useSelector } from "react-redux";
import { activeInstanceStatus, ClientStatus } from "../../../redux/clientSlice";

const ClientHealthSentinel = (): React.ReactElement => {
  const status: ClientStatus = useSelector(activeInstanceStatus);

  const handleOnClose = () => {
    window.location.href = "/login";
  };

  if (status.state !== 'healthy') {
    return (
      <div>
        <h3>Your session has expired</h3>
        <p>Your current session has expired. Please, sign in and try again.</p>
        <button onClick={handleOnClose}>Login</button>
      </div>
    )
  } else {
    return <></>
  }
}

export default ClientHealthSentinel
