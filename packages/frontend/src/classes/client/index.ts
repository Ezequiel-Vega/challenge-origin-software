export type User = {
  username: string;
  password: string;
}

export type StockMarket = {
  ID?: number;
  name: string;
  symbol: string;
  currency: string;
}

export type AccountInfo = {
  user: string;
  token: string;
}

export type ErrorInfo = {
  msg?: string;
  fields?: Map<string, string>;
};

export default interface Client {
  fetchAccountInfo(): Promise<AccountInfo>;

  addStockMarket(stockMarket: StockMarket): Promise<void>;

  fetchStockMarketByName(name: string): Promise<StockMarket>;
  fetchAllStockMarkets(): Promise<StockMarket[]>;

  deleteStockMarket(id: number): Promise<void>;

  onSessionExpired(callback: () => void): () => void;
}
