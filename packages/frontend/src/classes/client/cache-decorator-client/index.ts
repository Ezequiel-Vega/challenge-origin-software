import Client, { AccountInfo, StockMarket } from '..'

export default class CacheDecoratorClient implements Client {
  private client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  async fetchAccountInfo(): Promise<AccountInfo> {
    return this.client.fetchAccountInfo();
  }

  async addStockMarket(stockMarket: StockMarket): Promise<void> {
    await this.client.addStockMarket(stockMarket);
  }

  async fetchStockMarketByName(name: string): Promise<StockMarket> {
    return this.client.fetchStockMarketByName(name);
  }

  async fetchAllStockMarkets(): Promise<StockMarket[]> {
    return this.client.fetchAllStockMarkets();
  }

  async deleteStockMarket(id: number): Promise<void> {
    return this.client.deleteStockMarket(id);
  }

  onSessionExpired(callback: () => void): () => void {
    return this.client.onSessionExpired(callback);
  }
}
