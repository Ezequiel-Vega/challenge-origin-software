import { createSlice } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';
import { useQuery } from 'react-query';
import { RootState } from './rootReducer'
import AppConfig from '../classes/app-config';
import Client, { AccountInfo, ErrorInfo } from '../classes/client';

export interface ClientStatus {
  state: 'healthy' | 'session-expired';
  msg?: string;
}

export interface ClientState {
  instance: Client;
  status: ClientStatus;
}

const initialState: ClientState = {
  instance: AppConfig.buildClient(),
  status: {
    state: 'healthy'
  }
};

export const clientSlice = createSlice({
  name: 'client',
  initialState: initialState,
  reducers: {
    sessionExpired(state) {
      state.status = {
        state: 'session-expired',
        msg: 'Sessions has expired. You need to login again',
      };
    }
  }
});

export const fetchAccount = (): AccountInfo | undefined => {
  const client = useSelector(activeInstance);
  const { data } = useQuery<unknown, ErrorInfo, AccountInfo>('account', () => {
    return client.fetchAccountInfo();
  });
  return data;
}

export const activeInstance = (state: RootState): Client => {
  return state.client.instance;
};

export const activeInstanceStatus = (state: RootState): ClientStatus => {
  return state.client.status;
};

export const { sessionExpired } = clientSlice.actions;
export default clientSlice.reducer;
