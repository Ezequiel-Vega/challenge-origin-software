import { combineReducers } from '@reduxjs/toolkit';
import clientReducer from './clientSlice';

const rootReducer = combineReducers({
    client: clientReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
