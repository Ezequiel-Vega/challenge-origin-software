export const validatePassword = (value: string): boolean | string => {
  if (value.length < 6) {
    return "Password must be at least 6 characters";
  }
  if (value.length > 16) {
    return "Password must be less than 16 characters";
  }
  if (!value.match(/^[a-zA-Z0-9_-]+$/)) {
    return "Password must only contain letters, numbers, underscores and dashes";
  }
  return true;
}

export const validateUser = (value: string): boolean | string => {
  if (value.length < 4) {
    return "Username must be at least 4 characters";
  }
  if (value.length > 40) {
    return "Username must be less than 40 characters";
  }
  if (!value.match(/^[a-zA-Z0-9]+$/)) {
    return "Username must only contain letters and numbers";
  }
  return true;
}
