import React from 'react'
import { ErrorOption } from 'react-hook-form';
import GlobalError from '../global-error';

type ErrorProps = {
  username?: ErrorOption
  password?: ErrorOption
}

const Errors = (props: ErrorProps) => {
  const {username, password} = props;
  return (
    <>
      {
        username?.type === 'maxLength' && <GlobalError msg='The Username is too long'/> ||
        username?.type === 'required' && <GlobalError msg='The Username is required'/> ||
        username?.type === 'minLength' && <GlobalError msg='The Username is too short'/> ||
        username?.type === 'pattern' && <GlobalError msg='The Username is invalid'/> ||

        password?.type === 'maxLength' && <GlobalError msg='The password is too long'/> ||
        password?.type === 'required' && <GlobalError msg='The password is required'/> ||
        password?.type === 'minLength' && <GlobalError msg='The password is too short'/> ||
        password?.type === 'pattern' && <GlobalError msg='The password is invalid'/>
      }
    </>
  )
}

export default Errors
