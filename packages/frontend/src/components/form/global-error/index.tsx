import React from 'react';
import './style.css';

type GlobalErrorProp = {
  msg?: string;
};

const GlobalError = (prop: GlobalErrorProp) => {
  const hasError = Boolean(prop?.msg);

  return hasError ? (
    <div className='alert'>
      {prop?.msg}
    </div>
  ) : null;
}

export default GlobalError
