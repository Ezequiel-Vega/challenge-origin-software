import React, { useState, useEffect } from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { useSearchParams, Link } from "react-router-dom";
import axios from 'axios';

const Graphic = () => {
  const [getParam] = useSearchParams();
  const [optionsGraphic, setOptionsGraphic] = useState({});
  const [data, setData] = useState({
    interval: 5,
    dateFrom: "2021-04-16%2009:48:00",
    dateTo: "2021-04-16%2019:48:00",
    symbol: getParam.get('symbol') || "AAPL"
  });

  useEffect(() => {
    axios
    .get(
      `https://api.twelvedata.com/time_series?symbol=${data.symbol}&interval=${data.interval}min&start_date=${data.dateFrom}&end_date=${data.dateTo}&apikey=59e0618e28c74bc6836f1c15b9c4b406`
    )
    .then((res) => {
      console.log(res)
      const price = res.data.values.map((values: any) =>
        parseFloat(values.open)
      );

      const period = res.data.values.map((values: any) => values.datetime.split(' ')[1]);

      setOptionsGraphic({
        title: {
          text: data.symbol,
        },
        yAxis: {
          title: {
            text: "Price",
          },
        },
        xAxis: {
          title: {
            text: "days",
          },
          categories: period
        },
        series: [
          {
            name: data.symbol,
            data: price,
          },
        ],
      });
    });
  }, [data])

  return <>
    <Link to="/">Go Back...</Link>
    <HighchartsReact highcharts={Highcharts} options={optionsGraphic} />
    <div className="graphic-controls">
      <div className="graphic-controls-item">
        <label>Interval</label>
        <select
          value={data.interval}
          onChange={(e) => {
              setData({ ...data, interval: Number(e.target.value) });
            }
          }
        >
          <option value={1}>1 minuto</option>
          <option value={5}>5 minutos</option>
          <option value={15}>15 minutos</option>
        </select>
      </div>
    </div>
  </>;
};

export default Graphic;
