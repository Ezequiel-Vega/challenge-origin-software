import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import Client, { ErrorInfo, StockMarket } from "../../classes/client";
import { activeInstance } from "../../redux/clientSlice";

const Dashboard = (props?: any) => {
  const client: Client = useSelector(activeInstance);
  const [data, setData] = React.useState<StockMarket[]>([]);
  const [stockMarkets, setStockMarkets] = React.useState<any[]>([]);
  const { register, handleSubmit, formState: {errors} } = useForm();

  useEffect(() => {
    if (client) {
      client.fetchAllStockMarkets().then((stockMarkets) => {
        setData(stockMarkets);
      }).catch((error: ErrorInfo) => {
        console.error(error);
      });
    }
  }, [data]);

  const deleteStockMarket = (id: number | undefined): void => {
    if (!id) {
      return;
    }

    client.deleteStockMarket(id)
      .then(() => {
        setData(data.filter((stockMarket) => stockMarket.ID !== id));
      });
  }

  const checKeyPressed = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const symbol = event.target.value;
    const response = await axios.get(`https://api.twelvedata.com/stocks?symbol=${symbol ? symbol : '0'}&source=docs`)
    setStockMarkets(response.data.data);
  }

  const convertToString = (value: StockMarket) => {
    return JSON.stringify(value);
  }

  const addNewStockMarket = async (data: any) => {
    const parseToJson = await JSON.parse(data.symbol);
    client.addStockMarket(parseToJson);
  }

  return (
    <>
      <h2>Mis Acciones</h2>

      <div>
        <form onSubmit={handleSubmit(addNewStockMarket)}>
          <label htmlFor="symbol">Symbol</label>
          <input type="text" id="symbol" placeholder="Search stock markets" onChange={checKeyPressed} />
          <select name="searchStockMarket" id="symbol">
            {stockMarkets.map((stockMarket) => {
              return <option key={stockMarket.exchange + stockMarket.name + stockMarket.symbol} value={convertToString(
                {
                  symbol: stockMarket.symbol,
                  name: stockMarket.name,
                  currency: stockMarket.currency
                }
              )}
                {...register('symbol')}>{stockMarket.symbol} - {stockMarket.name}</option>
            })}
          </select>
          <button type="submit">Add Symbol</button>
        </form>
      </div>

      <table>
        <thead>
          <tr>
            <th>Symbol</th>
            <th>Name</th>
            <th>Currency</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {data?.map((stockMarket) => (
            <tr key={stockMarket.ID}>
              <td><Link to={`/graphic?symbol=${stockMarket.symbol}`}>{stockMarket.symbol}</Link></td>
              <td>{stockMarket.name}</td>
              <td>{stockMarket.currency}</td>
              <td><button onClick={() => deleteStockMarket(stockMarket.ID)} >Delete</button></td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Dashboard;
