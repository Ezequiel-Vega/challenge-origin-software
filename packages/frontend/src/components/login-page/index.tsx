import React, {useState} from 'react'
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { User } from '../../classes/client';
import { validateUser, validatePassword } from '../form/validators/validatorForms';
import Errors from '../form/errors/errorsForm';
import GlobalError from '../form/global-error';

type ErrorResponse = {
  message: string;
  error: boolean;
}

const LoginForm = () => {
  const [errorResponse, setErrorResponse] = useState<ErrorResponse>({ message: '', error: false });
  const { register, handleSubmit, formState: {errors} } = useForm<User>();

  const onSubmit = (data: User) => {
    axios.post('http://localhost:8000/api/v1/signin', {
      username: data.username,
      password: data.password
    })
    .then(res => {
      if (!res.data.error) {
        localStorage.setItem('token', res.data.data);
        window.location.href = '/';
      }
    })
    .catch(err => {
      if (err.response.status === 500 && err.response.data.error) {
        setErrorResponse({ message: err.response.data.message, error: true });
      }
    });
  }

  return (
    <div style={{"width": "300px", "margin": "0 auto"}}>
      <h2>Login</h2>

      {errorResponse.error && <GlobalError msg={errorResponse.message} />}

      <form onSubmit={handleSubmit(onSubmit)}>
        <Errors username={errors.username} password={errors.password} />

        <input type="text" placeholder='Username' {...register("username", {
          required: true,
          minLength: 4,
          maxLength: 40,
          pattern: /^[a-zA-Z0-9]+$/,
          validate: validateUser
        })} />

        <input type="password" placeholder='Password' {...register("password", {
          required: true,
          minLength: 6,
          maxLength: 16,
          pattern: /^[a-zA-Z0-9_-]+$/,
          validate: validatePassword
        })} />
        <button type='submit'>Sign In</button>
      </form>

      <Link to="/register">Register</Link>
    </div>
  )
}

export default LoginForm
