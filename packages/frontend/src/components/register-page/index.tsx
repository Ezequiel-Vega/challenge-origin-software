import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { validateUser, validatePassword } from '../form/validators/validatorForms';
import { User } from '../../classes/client';
import Errors from '../form/errors/errorsForm';
import GlobalError from '../form/global-error';

type ErrorResponse = {
  message: string;
  error: boolean;
}

const RegisterForm = () => {
  const [errorResponse, setErrorResponse] = useState<ErrorResponse>({ message: '', error: false });
  const { register, handleSubmit, formState: {errors} } = useForm<User>();

  const onSubmit = (data: User) => {
    axios.post('http://localhost:8000/api/v1/signup', {
      username: data.username,
      password: data.password
    })
    .then(res => {
      if (!res.data.error) {
        localStorage.setItem('token', res.data.data);
        window.location.href = '/login';
      }
    })
    .catch(err => {
      if (err.response.status === 500 && err.response.data.error) {
        setErrorResponse({ message: err.response.data.message, error: true });
      }
    });
  }

  return (
    <div style={{"width": "300px", "margin": "0 auto"}}>
      <h2>Register</h2>

      {errorResponse.error && <GlobalError msg={errorResponse.message} />}

      <form onSubmit={handleSubmit(onSubmit)}>
        <Errors username={errors.username} password={errors.password} />

        <input type="text" placeholder='Username' {...register("username", {
          required: true,
          minLength: 4,
          maxLength: 40,
          pattern: /^[a-zA-Z0-9]+$/,
          validate: validateUser
        })} />

        <input type="password" placeholder='Password' {...register("password", {
          required: true,
          minLength: 6,
          maxLength: 16,
          pattern: /^[a-zA-Z0-9_-]+$/,
          validate: validatePassword
        })} />
        <button type='submit'>Register</button>
      </form>
    </div>
  )
}

export default RegisterForm
