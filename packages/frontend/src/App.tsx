import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';

import RegisterForm from './components/register-page';
import LoginForm from './components/login-page';
import Dashboard from './components/dashboard';
import Graphic from './components/graphic';
import withSessionExpirationHandling from './components/HOCs/withSessionExpirationHandling';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchIntervalInBackground: false,
      staleTime: 1 * 1000 * 60,
    }
  }
});

const App: React.FC = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Routes>
        <Route path="/">
          <Route index element={withSessionExpirationHandling(Dashboard)}/>
          <Route path="login" element={<LoginForm/>} />
          <Route path="register" element={<RegisterForm/>} />
          <Route path="graphic" element={<Graphic/>} />
        </Route>
      </Routes>
    </QueryClientProvider>
  )
}

export default App;
