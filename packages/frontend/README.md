# Frontend

Frontend application for stock monitoring

## Quick Start
**This project is created with yarn**

### Develop

1) Install dependencies

```shell
yarn install
```

2) Start app

```shell
yarn start
```
