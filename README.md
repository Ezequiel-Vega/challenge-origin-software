# Challenge

Monitoring application. This project is created with lerna and monorepo architecture

## Quick Start

1) Install dependencies

```shell
yarn install
```

2) Start app

```shell
yarn start
```
